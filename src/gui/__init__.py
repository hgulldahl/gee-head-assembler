﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2012 Håvard Gulldahl
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

__author__ = 'havard@gulldahl.no'

import sys

from PyQt4 import uic, QtCore, QtGui

import geeha_ui
#import geeha_rc

class StatusBox(QtGui.QWidget):
    INFO = 1
    WARNING = 2
    ERROR = 3

    def __init__(self, msg, autoclose=True, msgtype=None, parent=None):
        """autoclose may be a boolean (True == autoclose) or a signal that we
        connect our close() method to"""
        super(StatusBox, self).__init__(parent)
        self.parent = parent
        self.autoclose = autoclose
        self.autoclosetimeout = 1000
        self.setWindowFlags(QtCore.Qt.Popup)
        if msgtype in (None, self.INFO):
            bgcolor = '#ffff7f'
        elif msgtype == self.WARNING:
            bgcolor = 'blue'#'#ffff7f'
        elif msgtype == self.ERROR:
            bgcolor = 'red'#'#ffff7f'
            self.autoclosetimeout = 3000

        self.setStyleSheet(u'QWidget { background-color: %s; }' % bgcolor)
        layout = QtGui.QVBoxLayout(self)
        s = QtGui.QLabel(msg, self)
        layout.addWidget(s)

    def show_(self):
        if self.autoclose == True:
            QtCore.QTimer.singleShot(self.autoclosetimeout, self.close)
        elif hasattr(self.autoclose, 'connect'): # it's a qt/pyqt signal
            self.autoclose.connect(self.close)
        self.show()

    def delete_(self):
        self.hide()
        self.deleteLater()

    def close(self):
        anim = QtCore.QPropertyAnimation(self, "windowOpacity", self.parent)
        anim.setDuration(1000)
        anim.setStartValue(1.0)
        anim.setEndValue(0.0)
        anim.finished.connect(self.delete_)
        self.anim = anim
        self.anim.start()

def readResourceFile(qrcPath):
    """Read qrc file and return QString.

    'qrcPath' is ':/path/name', for example ':/txt/about.html'
    """
    f = QtCore.QFile(qrcPath)
    if not f.open(QtCore.QIODevice.ReadOnly | QtCore.QIODevice.Text):
        raise IOError(u"Could not read resource '%s'" % qrcPath)
    t = QtCore.QTextStream(f)
    t.setCodec("UTF-8")
    s = QtCore.QString(t.readAll())
    f.close()
    return s

class SceneEvent(QtGui.QGraphicsTextItem):
    def __init__(self, calevent, parent=None):
        super(SceneEvent, self).__init__(parent)
        self.cal = calevent
        self.setTextWidth(50)
        self.setPlainText(calevent)

    def paint(self, painter, option, widget):
        painter.drawRoundedRect(0, 0, 55, 20, 5, 5)
        super(SceneEvent, self).paint(painter, option, widget)

        
class Geeha(QtGui.QMainWindow):
    msg = QtCore.pyqtSignal(unicode, name="msg")
    loaded = QtCore.pyqtSignal()

    def __init__(self, app, language='no', parent=None):
        super(Geeha, self).__init__(parent)
        self.app = app
        self.workers = []
        self.events = []
        self.translator = None
        self.settings = QtCore.QSettings('lurtgjort.no', 'Geeha')
    	self.ui = geeha_ui.Ui_MainWindow()
        #self.setLanguage(language)
        self.ui.setupUi(self)
        self.scene = QtGui.QGraphicsScene(self)
        self.ui.graphicsView.setScene(self.scene)
        self.addEvent("hei")
        print self.ui.graphicsView.items()        

    def run(self):
        self.show()
        sys.exit(self.app.exec_())

    def setLanguage(self, language):
        if self.translator is not None:
            self.app.removeTranslator(self.translator)
        else:
            self.translator = QtCore.QTranslator(self.app)
        print "loading translation: odometer_%s" % language
        self.translator.load(':data/translation_%s' % language)
        self.app.installTranslator(self.translator)    

    def addEvent(self, event):
        e = SceneEvent(event)
        self.scene.addItem(e)
        self.events.append(e)
        
def runGui(argv):
    if sys.platform == 'win32':
        # default win32 looks awful, make it pretty
        # docs advise to do this before QApplication() is started
        QtGui.QApplication.setStyle("cleanlooks") 
    app = QtGui.QApplication(argv)
    if sys.platform == 'win32':
        def setfont(fontname):
            app.setFont(QtGui.QFont(fontname, 9))
            return unicode(app.font().toString()).split(',')[0] == fontname
        # default win32 looks awful, make it pretty
        for z in ['Lucida Sans Unicode', 'Arial Unicode MS', 'Verdana']:
            if setfont(z): break
    g = Geeha(app)
    g.run()


if __name__ == '__main__':
    runGui(sys.argv)
