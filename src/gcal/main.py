﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2010 Google Inc.
# Copyright (C) 2012 Håvard Gulldahl
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

__author__ = 'havard@gulldahl.no'

import gflags
import httplib2
import logging
import os
import pprint
import sys
import datetime
import cPickle as pickle

from apiclient.discovery import build
from oauth2client.file import Storage
from oauth2client.client import AccessTokenRefreshError
from oauth2client.client import flow_from_clientsecrets
from oauth2client.tools import run

class CalendarList(object):
    def __init__(self, service, http):
        self.service = service
        self.http = http
    
    def list(self):
        _calendars = self.service.calendarList().list().execute(self.http)
        return [Calendar(cal, self.service, self.http) for cal in _calendars['items']]
        
class Calendar(object):
    def __init__(self, cal, service, http):
        self.service = service
        self.http = http
        if isinstance(cal, dict):
            self.__dict__.update(cal)
        elif isinstance(cal, basestring):
            _caldict = service.calendars().get(calendarId=cal).execute(http)
            self.__dict__.update(_caldict)
        
    def __str__(self):
        return '%s (%s)' % (self.summary, self.id)
     
    def events(self, since=None, until=None):
        _extras = {}
        if isinstance(since, datetime.datetime):
            _extras['timeMin'] = since.isoformat()
        if isinstance(until, datetime.datetime):
            _extras['timeMax'] = until.isoformat()
        _events = self.service.events().list(calendarId=self.id,
                                             maxResults=200,
                                             **_extras).execute(self.http)
        return [Event(ev, self.service, self.http) for ev in _events['items']]

    def addEvent(self, desc, who, since, until):
        if isinstance(until, datetime.datetime):
            _end = until.date().isoformat()
            _duration = until-since
        elif isinstance(until, datetime.timedelta):
            _duration = until
            _end = (since+until).isoformat()
        elif isinstance(until, datetime.date):
            _end = until.isoformat()
            _duration = until-since
            
        _event = dict(start=dict(date=since.isoformat()),
                      end=dict(date=_end),
                      summary='%s: %s, %s' % (who, desc, until),
                      extendedProperties=dict(private=dict(geeha_who=pickle.dumps(who),
                                                           geeha_desc=pickle.dumps(desc),
                                                           geeha_until=pickle.dumps(until))),
                      )
        e = self.service.events().insert(calendarId=self.id,
                                         body=_event).execute(self.http)        
        return Event(e, self.service, self.http)
        
class Event(object):
    def __init__(self, eventdict, service, http):
        self.service = service
        self.http = http
        self.__dict__.update(eventdict)
        
    def __str__(self):
        return '%s: %s -> %s' % (self.id, self.start, self.end)

FLAGS = gflags.FLAGS

# CLIENT_SECRETS, name of a file containing the OAuth 2.0 information for this
# application, including client_id and client_secret, which are found
# on the API Access tab on the Google APIs
# Console <http://code.google.com/apis/console>
CLIENT_SECRETS = 'client_secrets.json'

# Helpful message to display in the browser if the CLIENT_SECRETS file
# is missing.
MISSING_CLIENT_SECRETS_MESSAGE = """
WARNING: Please configure OAuth 2.0

To make this sample run you will need to populate the client_secrets.json file
found at:

   %s

with information from the APIs Console <https://code.google.com/apis/console>.

""" % os.path.join(os.path.dirname(__file__), CLIENT_SECRETS)

# Set up a Flow object to be used if we need to authenticate.
FLOW = flow_from_clientsecrets(CLIENT_SECRETS,
    scope='https://www.googleapis.com/auth/calendar',
    message=MISSING_CLIENT_SECRETS_MESSAGE)


# The gflags module makes defining command-line options easy for
# applications. Run this program with the '--help' argument to see
# all the flags that it understands.
gflags.DEFINE_enum('logging_level', 'ERROR',
    ['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
    'Set the level of logging detail.')


def main(argv):
  # Let the gflags module process the command-line arguments
  try:
    argv = FLAGS(argv)
  except gflags.FlagsError, e:
    print '%s\\nUsage: %s ARGS\\n%s' % (e, argv[0], FLAGS)
    sys.exit(1)

  # Set the logging according to the command-line flag
  logging.getLogger().setLevel(getattr(logging, FLAGS.logging_level))

  # If the Credentials don't exist or are invalid run through the native client
  # flow. The Storage object will ensure that if successful the good
  # Credentials will get written back to a file.
  storage = Storage('calendar.dat')
  credentials = storage.get()

  if credentials is None or credentials.invalid:
    credentials = run(FLOW, storage)

  # Create an httplib2.Http object to handle our HTTP requests and authorize it
  # with our good Credentials.
  http = httplib2.Http()
  http = credentials.authorize(http)

  service = build("calendar", "v3", http=http)

  # calendars = CalendarList(service, http).list()
  # for c in calendars:
    # print c
    
  mycal = Calendar('lurtgjort.no_m4o5bk2cs4bae55hp26b837l9o@group.calendar.google.com', service, http)
  for e in mycal.events():
    print e
    print e.attendees
  # myevent = mycal.addEvent('klippe fiskehund', 'SÅI', datetime.date.today(), datetime.timedelta(days=7))
  # print myevent  
  
  try:
    # person = service.people().get(userId='me').execute(http)
    # calendars = service.calendarList().list().execute(http)

    # from pprint import pprint
    print "Got calendars"
    # pprint(calendars)
    # print "Got your ID: %s" % person['displayName']
    # print
    # print "%-040s -> %s" % ("[Activitity ID]", "[Content]")

    # # Don't execute the request until we reach the paging loop below
    # request = service.activities().list(
        # userId=person['id'], collection='public')
    # # Loop over every activity and print the ID and a short snippet of content.
    # while ( request != None ):
      # activities_doc = request.execute()
      # for item in activities_doc.get('items', []):
        # print '%-040s -> %s' % (item['id'], item['object']['content'][:30])

      # request = service.activities().list_next(request, activities_doc)

  except AccessTokenRefreshError:
    print ("The credentials have been revoked or expired, please re-run"
      "the application to re-authorize")

if __name__ == '__main__':
  main(sys.argv)